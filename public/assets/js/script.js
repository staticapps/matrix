var c = document.getElementById("c");
var ctx = c.getContext("2d");

// making the canvas full screen
c.height = window.screen.height;
c.width = window.screen.width;

// the characters
var gurmukhi = "੧੨੩੪੫੬੭੮੯੦ੳਅਰਤਯਪਸਦਗਹਜਕਲਙੜਚਵਬਨਮੲਥਫਸ਼ਧਘਝਖਲ਼ੜ੍ਹਛਭਣ"
var sanskrit = "१२३४५६७८९अरतयपसदगहजकलङषचवबनमआथय़फशधघझखळक्षछभणऒ"
// Katakana symbols got from: https://scifi.stackexchange.com/a/182823
var katakana = "ﾊﾐﾋｰｳｼﾅﾓﾆｻﾘｱﾎﾃﾏｹﾒｴｶｷﾑﾕﾗｾﾈｽﾀﾇﾍ"
// converting the string into an array of single characters
var characters = katakana.split("");
var font_size = 18;
var font_name = "Embedded Font";
var columns = c.width/font_size;    // number of columns for the rain
var color_mode = 1; // 1: Matrix (Green text on Black), 2: Switching

// an array of drops - one per column
var drops = [];
// x below is the x coordinate
// 1 = y-coordinate of the drop (same for every drop initially)
for (var x = 0; x < columns; x++) 
    drops[x] = 1;

function getColor() {
    if (color_mode==1) {
        return "rgba(0, 0, 0, 0.05)";
    } else {
        return "rgba(" + moment().format('HH') + ","
                + moment().format('mm') + ","
                + moment().format('ss')  + ", 0.05)";
    }
}

function getColorHex() {
    return "#" + moment().format('HHmmss');   
}

// drawing the characters
function draw() {
    $("#color").css("background", getColorHex());
    $('#color').html(getColorHex());
    
    // Get the BG color based on the current time i.e. rgb(hh, mm, ss)
    // translucent BG to show trail
    ctx.fillStyle = getColor();
    ctx.fillRect(0, 0, c.width, c.height);
    
    if (color_mode==1) {
        ctx.fillStyle = "#0a0"; // matrix Green text
    } else if (color_mode==2) {
        ctx.fillStyle = "#BBB"; // grey text
    }
    ctx.font = font_size + "px " + font_name;
    
    // looping over drops
    for (var i = 0; i < drops.length; i++) {
        // a random chinese character to print
        var text = characters[Math.floor(Math.random() * characters.length)];
        // x = i * font_size, y = value of drops[i] * font_size
        ctx.fillText(text, i * font_size, drops[i] * font_size);
        
        // sending the drop back to the top randomly after it has crossed the screen
        // adding randomness to the reset to make the drops scattered on the Y axis
        if (drops[i] * font_size > c.height && Math.random() > 0.975)
            drops[i] = 0;
        
        // Incrementing Y coordinate
        drops[i]++;
    }
}



setInterval(draw, 33);


$(function() {

    // Autohide color/panel
    $(document).idle({
        onIdle: function(){
            $('#panel').fadeOut();
        },
        onActive: function(){
            $('#panel').fadeIn(200);
        },
        idle: 6000
    });

    if ($.fullscreen.isNativelySupported()) {
        $('#fullscreen').show();
        $('#fullscreen').click(function(){
            if ($.fullscreen.isFullScreen()) {
                $.fullscreen.exit();
            } else {
                $('body').fullscreen();
            }
        });
    }

    $('#togglecolor').click(function(){
        color_mode=(color_mode==1) ? 2 : 1;
    });

});

