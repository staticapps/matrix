# Matrix Animation

Matrix rain animation inspired by the movie Matrix that you can use right inside your browser. It is the most lightweight low-resource animation project that I could find.

Forked from: [https://github.com/parambirs/matrix](https://github.com/parambirs/matrix) \
License: Not found, but code available

Changes applied:
- Moved css+js files on better organized directories
- Set the size of the canvas to screen size, so that it does not cut of animation on resize
- Changed characters in the animation, added font_name variable for ease of editing, added built in version of Bokutachi no Gothic 2 Regular font ([font license info](https://www.freejapanesefont.com/bokutachi-no-gothic-2-regular-download/))
- Added autohide behavior for color, added `panel` container div, autohide mouse pointer
- Added toggle fullscreen feature


# Use Online

- [https://staticapps.gitlab.io/matrix](https://staticapps.gitlab.io/matrix)


# Deployment on LBRY

Install [lpack-bash](https://gitlab.com/funapplic66/lpack-bash) then run:

```
lpack public matrix-xyz.lbry
```

Then upload the resulting `.lbry` file to [LBRY](https://lbry.tv).
